<?php

namespace SamDemo\CustomerStatus\CustomerData;

use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Customer\CustomerData\SectionSourceInterface;

/**
 * Custom-status section
 */
class Status implements SectionSourceInterface
{
    /**
     * @var \Magento\Customer\Helper\Session\CurrentCustomer
     */
    private $currentCustomer;

    /**
     * Init dependencies.
     *
     * @param \Magento\Customer\Helper\Session\CurrentCustomer $currentCustomer
     */
    public function __construct(
        CurrentCustomer $currentCustomer
    ) {
        $this->currentCustomer = $currentCustomer;
    }

    /**
     * {@inheritdoc}
     */
    public function getSectionData()
    {
        if (!$this->currentCustomer->getCustomerId()) {
            return [];
        }

        $customer = $this->currentCustomer->getCustomer();
        /** @var \Magento\Framework\Api\AttributeValue $customStatus */
        $customStatus = $customer->getCustomAttribute('custom_status');
        $value = (isset($customStatus)) ? $customStatus->getValue(): null;
        return [
            'custom_status' => $value,
        ];
    }
}
