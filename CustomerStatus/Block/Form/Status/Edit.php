<?php

namespace SamDemo\CustomerStatus\Block\Form\Status;

/**
 * Custom-status attribute edit form block
 */
class Edit extends \Magento\Customer\Block\Account\Dashboard
{
    /**
     * Get current custom-status attribute value
     *
     * @return string|null
     */
    public function getCustomStatus(){
        $customer = $this->customerRepository->getById($this->customerSession->getCustomerId());
        /** @var \Magento\Framework\Api\AttributeValue $customStatus */
        $customStatus = $customer->getCustomAttribute('custom_status');
        return (isset($customStatus)) ? $customStatus->getValue(): null;
    }
}
